import {configureStore} from "@reduxjs/toolkit";
import loggedInReducer from "../features/counter/loggedInSlice";
import registerReducer from "../features/counter/registerSlice";
import popUpReducer from "../features/counter/popUpSlice";


export const store = configureStore({
    reducer: {
        isLogged: loggedInReducer,
        register: registerReducer,
        popUp: popUpReducer,
    },
},);


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;