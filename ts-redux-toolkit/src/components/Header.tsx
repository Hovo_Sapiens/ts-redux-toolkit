import React from "react";
import LoginOrRegister from "./LoginOrRegister";
import LogOut from "./LogOut";
import {useAppSelector} from "../app/hooks";


const Header:React.FC = () => {

    const isLogged = useAppSelector((state) => state.isLogged);

    // function renderLogInOrLogOut (): React.FC {
    //     if(isLogged.isLogged) {
    //         return <LoginOrRegister/>;
    //     } else {
    //         return <LogOut/>;
    //     }
    // }

    return (
        <div className={"header"}>
            <h1 className={"headerH1"}>TypeScript Redux Toolkit</h1>
            {isLogged.isLogged ? <LogOut/> : <LoginOrRegister/>}
        </div>
    )
}

export default Header;