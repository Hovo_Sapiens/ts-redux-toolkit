import {useAppDispatch, useAppSelector} from "../app/hooks";
import SingleFetchElement from "./SingleFetchElement";
import {removeFromFavorites} from "../features/counter/registerSlice";


function FavoritesList () {

    const usersList = useAppSelector((state) => state.register);
    const userIndex = useAppSelector((state) => state.isLogged.userIndex);
    const dispatch = useAppDispatch();


    function removeBtnHandler (title:string) {
        dispatch(removeFromFavorites({index: userIndex, list: title}))
    }

    return (
        <div className="favoritesList">
            {
                usersList[userIndex]?.favoritesList.map((title) => {
                    return <SingleFetchElement title={title}  key={title} btnInnerText="Remove"
                                               btnHandler={() => {removeBtnHandler(title)}}/>
                })
            }
        </div>
    )
}


export default FavoritesList;