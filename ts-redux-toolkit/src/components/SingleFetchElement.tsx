import {FetchSingleObj} from "../interfacesTypesEnums/interfaces";
import React from "react";
// import {useAppDispatch, useAppSelector} from "../app/hooks";
// import {addToFavorites} from "../features/counter/registerSlice";


const SingleFetchElement: React.FC<FetchSingleObj> = ({completed, id, title, userId, btnHandler, btnInnerText}) =>  {

    // const dispatch = useAppDispatch();
    // const isLogged = useAppSelector((state) => state.isLogged)
    //
    // function addBtnHandler () {
    //     dispatch(addToFavorites({index: isLogged.userIndex, list: title}))
    // }


    return (
        <div className="singleFetchEl">
            <p>Title : {title}</p>
            <button className="btn" onClick={() => {btnHandler(title)}}>{btnInnerText}</button>
        </div>
    )
}

export default SingleFetchElement;