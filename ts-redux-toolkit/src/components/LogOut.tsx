import React from "react";
import {useAppDispatch} from "../app/hooks";
import {logOut} from "../features/counter/loggedInSlice";


function LogOut () {

    const dispatch = useAppDispatch();

    function logOutHandler (): undefined {
        dispatch(logOut());
        return;
    }

    return (
        <div className="logOut">
            <button className="btn" onClick={logOutHandler}>Log out</button>
        </div>
    )
}

export default LogOut;