import {useAppDispatch, useAppSelector} from "../app/hooks";
import {showPopUp} from "../features/counter/popUpSlice";


function PopUp () {

    const dispatch = useAppDispatch();
    const popUpText = useAppSelector(state => state.popUp.textPopUp)

    function popUpBtnHandler () {
        dispatch(showPopUp({textPopUp: "", show: false}))
    }

    return (
        <div className="popUp">
            <div className="popUpInner">
                <p className="popUpTxt">{popUpText}</p>
                <button className="btn" onClick={popUpBtnHandler}>Cancel</button>
            </div>
        </div>
    )
}


export default PopUp;