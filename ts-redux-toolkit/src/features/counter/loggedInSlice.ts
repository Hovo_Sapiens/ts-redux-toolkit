import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";
import {IsUserLogged} from "../../interfacesTypesEnums/interfaces";



const initialState: IsUserLogged = {
    isLogged: false,
    userIndex: NaN,
}

export const loggedInSlice = createSlice(
    {
        name: "isLogged",
        initialState,
        reducers : {
            logIn: (state) => {state.isLogged = true},
            logOut: (state) => {state.isLogged = false},
            userIndex: (state, action:PayloadAction<number>) => {state.userIndex = action.payload}
            // incrementByAmount: (state, action: PayloadAction<number>) => {state.value += action.payload},
        },
    }
)

export const {logIn, logOut, userIndex} = loggedInSlice.actions;
export const selectCount = (state: RootState) => state.isLogged;

export default loggedInSlice.reducer;