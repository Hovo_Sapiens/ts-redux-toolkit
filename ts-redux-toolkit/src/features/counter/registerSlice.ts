import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {FavoritesAction, UserInfo} from "../../interfacesTypesEnums/interfaces";



const initialState: UserInfo[] = [];

export const registerSlice = createSlice(
    {
        name: "register",
        initialState,
        reducers : {
            register: (state, action:PayloadAction<UserInfo>) => {
                state.push(action.payload)
            },
            addToFavorites: (state, action:PayloadAction<FavoritesAction>) => {
                const List = state[action.payload.index]?.favoritesList ?? [];
                List[List.length] = action.payload.list;
                // console.log(state[action.payload.index].favoritesList);
            },
            removeFromFavorites: (state, action:PayloadAction<FavoritesAction>) => {
                let List = state[action.payload.index]?.favoritesList ?? [];
                state[action.payload.index].favoritesList = List.filter(function(value){return value !== action.payload.list})

            }
        },
    }
)

export const {register, addToFavorites, removeFromFavorites} = registerSlice.actions;

export default registerSlice.reducer;