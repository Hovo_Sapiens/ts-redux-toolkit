import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {PopUpInterface} from "../../interfacesTypesEnums/interfaces";

const initialState:PopUpInterface = {
    textPopUp: "",
    show: false,
}

export const popUpSlice = createSlice(
    {
        name: "popUp",
        initialState,
        reducers : {
           showPopUp: (state, action:PayloadAction<PopUpInterface>) =>
           {state.textPopUp = action.payload.textPopUp; state.show = action.payload.show},
        },
    }
)


export const {showPopUp} = popUpSlice.actions;

export default popUpSlice.reducer;