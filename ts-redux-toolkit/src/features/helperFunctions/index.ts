export function whiteSpaceCheck (stringToCheck: string): boolean {
    let checkValue = "";
    for(let i = 0; i < stringToCheck.length; ++i){
        if(stringToCheck[i] !== " ") {
            checkValue += stringToCheck[i];
        }
    }
    return checkValue.length !== 0;

}