import React from 'react';
import './styles/App.css';
import Header from "./components/Header";
import MainOutPut from "./components/MainOutPut";
import {useAppSelector} from "./app/hooks";
import FavoritesList from "./components/FavoritesList";
import PopUp from "./components/PopUp";

function App() {

    const isLogged = useAppSelector((state) => state.isLogged);
    const showPopUp = useAppSelector(state => state.popUp.show);

    const content = (
        <>
            <MainOutPut/>
            <FavoritesList/>
        </>
    )

    return (
        <div className={"app"}>
            <Header/>
            {isLogged.isLogged ? content: null}
            {showPopUp ? <PopUp/> : null}
        </div>
    )
}

export default App;
