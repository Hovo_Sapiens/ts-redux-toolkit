import {FetchSingleObj} from "../interfacesTypesEnums/interfaces";
import React from "react";
import {Typography, Button} from 'antd';
// import {useAppDispatch, useAppSelector} from "../app/hooks";
// import {addToFavorites} from "../features/counter/registerSlice";

const {Paragraph, Title} = Typography;


const SingleFetchElement: React.FC<FetchSingleObj> = ({completed, id, title, userId, btnHandler, btnInnerText}) =>  {

    // const dispatch = useAppDispatch();
    // const isLogged = useAppSelector((state) => state.isLogged)
    //
    // function addBtnHandler () {
    //     dispatch(addToFavorites({index: isLogged.userIndex, list: title}))
    // }


    return (
        <div className="singleFetchEl">
            <Paragraph italic strong className="paragraph"><Title level={4} underline>Title:</Title>{title}</Paragraph>
            <Button className="btn" onClick={() => {btnHandler(title)}} type="primary">{btnInnerText}</Button>
        </div>
    )
}

export default SingleFetchElement;