import {useAppDispatch, useAppSelector} from "../app/hooks";
import {showPopUp} from "../features/counter/popUpSlice";
import {Typography, Button} from 'antd';

const {Paragraph} = Typography;

function PopUp () {

    const dispatch = useAppDispatch();
    const popUpText = useAppSelector(state => state.popUp.textPopUp)

    function popUpBtnHandler () {
        dispatch(showPopUp({textPopUp: "", show: false}))
    }

    return (
        <div className="popUp">
            <div className="popUpInner">
                <Paragraph className="popUpTxt">{popUpText}</Paragraph>
                <Button className="btn" onClick={popUpBtnHandler} type="primary">Cancel</Button>
            </div>
        </div>
    )
}


export default PopUp;