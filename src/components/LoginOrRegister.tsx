import React, {useState} from "react";
import {whiteSpaceCheck} from "../features/helperFunctions";
import {register} from "../features/counter/registerSlice";
import {logIn, userIndex} from "../features/counter/loggedInSlice";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {UserInfo} from "../interfacesTypesEnums/interfaces";
import {showPopUp} from "../features/counter/popUpSlice";
import {Input, Button} from 'antd';


function LoginOrRegister() {

    const [loginAndPass, setLoginAndPass] = useState<UserInfo>({username: "", password: "", favoritesList: []});
    const {username, password} = loginAndPass;

    const dispatch = useAppDispatch();
    const usersList = useAppSelector((state) => state.register);


    function inputOnChangeHandler (e: React.ChangeEvent<HTMLInputElement>) {
        setLoginAndPass({...loginAndPass, [e.target.name]: e.target.value})
    }

    function loginBtnHandler (): undefined {
        if(username && password && whiteSpaceCheck(username) && whiteSpaceCheck(password)) {
            if(usersList.length > 0) {
                let searching = usersList.find(user => user.username === username && user.password === password);
                if(searching === undefined) {
                    dispatch(showPopUp({textPopUp: "Register first please", show: true}));
                    return;
                } else {
                    let userI = usersList.indexOf(searching)
                    dispatch(logIn());
                    dispatch(userIndex(userI));
                    return;
                }
            } else {
                dispatch(showPopUp({textPopUp: "Register first please", show: true}));
                return;
            }
        } else {
            dispatch(showPopUp({textPopUp: "Please fill all inputs", show: true}));
            return;
        }
    }

    function registerBtnHandler (): undefined {
        if(username && password && whiteSpaceCheck(username) && whiteSpaceCheck(password)) {
            if(usersList.length > 0) {
                let searching = usersList.find(user => user.username === username && user.password === password);
                if(searching === undefined) {
                    dispatch(register(loginAndPass));
                    setLoginAndPass({...loginAndPass, username: "", password: ""});
                    return;
                } else {
                    dispatch(showPopUp({textPopUp: "You are already registered", show: true}));
                    return;
                }
            } else {
                dispatch(register(loginAndPass));
                setLoginAndPass({...loginAndPass, username: "", password: ""});
                return;
            }

        } else {
            dispatch(showPopUp({textPopUp: "Please fill all inputs", show: true}));
            return;
        }
    }

    return (
        <div className={"loginOrRegister"}>
            <div>
                <Input type="text" placeholder="Username" name="username" value={loginAndPass.username}
                       onChange={(e) => {inputOnChangeHandler(e)}}/>
                <Input type="password" placeholder="Password" name="password" value={loginAndPass.password}
                       onChange={(e) => {inputOnChangeHandler(e)}}/>
            </div>
            <br/>
            <Button className={"loginBtn btn"} onClick={loginBtnHandler} type="primary">Log in</Button>
            <Button className={"registerBtn btn"} onClick={registerBtnHandler} type="primary">Register</Button>
        </div>
    )
}



export default LoginOrRegister;