import {useEffect, useState} from "react";
import {FetchSingleObj} from "../interfacesTypesEnums/interfaces";
import SingleFetchElement from "./SingleFetchElement";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {addToFavorites} from "../features/counter/registerSlice";
import {showPopUp} from "../features/counter/popUpSlice";
import {Spin} from 'antd';



function MainOutPut () {

    const [fetchList, setFetchList] = useState<FetchSingleObj[]>([]);

    const dispatch = useAppDispatch();
    const isLogged = useAppSelector((state) => state.isLogged)
    const usersList = useAppSelector((state) => state.register)

    function addBtnHandler (title:string) {
        if(usersList[isLogged.userIndex].favoritesList.indexOf(title) === -1) {
            dispatch(addToFavorites({index: isLogged.userIndex, list: title}))
        } else {
            dispatch(showPopUp({textPopUp: "Already added", show: true}));
        }
        return;
    }

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
            .then(json => setFetchList(json))
    }, [])




    return (
        <div className="mainOutPut">
            {
                fetchList.length > 0 ?
                fetchList.map((singleFetch) => {
                    return <SingleFetchElement title={singleFetch.title}  key={singleFetch.id} btnInnerText="Add to favorites"
                                               btnHandler={() => {addBtnHandler(singleFetch.title)}}/>
                }) : <Spin tip="Loading..." className="spin"/>
            }
        </div>
    )

}

export default MainOutPut;