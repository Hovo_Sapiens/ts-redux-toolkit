import React from "react";
import {useAppDispatch} from "../app/hooks";
import {logOut} from "../features/counter/loggedInSlice";
import {Button} from 'antd';


function LogOut () {

    const dispatch = useAppDispatch();

    function logOutHandler (): undefined {
        dispatch(logOut());
        return;
    }

    return (
        <div className="logOut">
            <Button className="btn" onClick={logOutHandler} type="primary">Log out</Button>
        </div>
    )
}

export default LogOut;