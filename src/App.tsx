import React from 'react';
import './styles/App.less';
// import 'antd/dist/antd.css';
// import Header from "./components/Header";
import MainOutPut from "./components/MainOutPut";
import FavoritesList from "./components/FavoritesList";
import PopUp from "./components/PopUp";
import {useAppSelector} from "./app/hooks";
import {Layout, Typography} from 'antd';
import LogOut from "./components/LogOut";
import LoginOrRegister from "./components/LoginOrRegister";

const {Header, Content} = Layout;
const {Title} = Typography;


function App() {

    const isLogged = useAppSelector((state) => state.isLogged);
    const showPopUp = useAppSelector(state => state.popUp.show);

    const content = (
        <>
            <MainOutPut/>
            <FavoritesList/>
        </>
    )

    return (
        <div className={"app"}>
            <Layout>
                <Header className="header" id="header">
                    <Title>TypeScript Redux Toolkit</Title>
                    {isLogged.isLogged ? <LogOut/> : <LoginOrRegister/>}
                </Header>
                <Content>
                    {isLogged.isLogged ? content: null}
                    {showPopUp ? <PopUp/> : null}
                </Content>
            </Layout>
        </div>
    )
}

export default App;
