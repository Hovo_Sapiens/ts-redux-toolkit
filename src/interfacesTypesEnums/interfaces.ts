import React from "react";

export interface UserInfo {
    username: string;
    password: string;
    favoritesList: string[];
}
export interface IsUserLogged {
    isLogged: boolean;
    userIndex: number;
}
export interface FetchSingleObj {
    completed?: boolean;
    id?: number;
    title: string;
    userId?: number;
    btnHandler: (title:string) => void;
    btnInnerText: string;
}
export interface FavoritesAction {
    index: number;
    list: string;
}
export interface PopUpInterface {
    textPopUp: string;
    show: boolean;
}

