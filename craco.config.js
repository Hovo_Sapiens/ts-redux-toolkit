const CracoAntDesignPlugin = require("craco-antd");


module.exports = {
    plugins: [
        {
            plugin: CracoAntDesignPlugin,
            options: {
                customizeTheme: {
                    "@primary-color": "#34eb77",
                    "@layout-header-background": "#006622",
                    "@font-size-base": "18px"
                }
            }
        },
    ]
};